package test.http2.jetty;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller
public class BlahController {

  @Get("/hello")
  String hello() {
    return "hello world";
  }

}
